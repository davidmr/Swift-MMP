<p align="center">
    <a href="https://github.com/johnlui/Swift-MMP"><img src="https://gitee.com/johnlui/Swift-MMP/raw/master/assets/Swift-MMP.jpg"></a>
</p>

<p align="center">
    <a href="https://github.com/johnlui/Swift-MMP"><img src="https://img.shields.io/badge/platform-ios-lightgrey.svg"></a>
    <a href="https://github.com/johnlui/Swift-MMP"><img src="https://img.shields.io/github/license/johnlui/Swift-MMP.svg?style=flat"></a>
    <a href="https://github.com/johnlui/Swift-MMP"><img src="https://img.shields.io/badge/language-Swift%203-orange.svg"></a>
    <a href="https://travis-ci.org/johnlui/Swift-MMP"><img src="https://img.shields.io/travis/johnlui/Swift-MMP.svg"></a>
</p>

### Github 地址：[https://github.com/johnlui/Swift-MMP](https://github.com/johnlui/Swift-MMP)

# 介绍

使用 Swift 语言编写的 Material Design 风格的 iOS 流媒体音乐播放器，简称 MMP。基于 [DOUAudioStreamer-Swift](https://github.com/johnlui/DOUAudioStreamer-Swift)。

## Features

- [x] 完全流媒体播放
- [x] 锁屏信息展示及控制
- [x] Apple Watch 展示及控制
- [x] 界面简介优雅

## 示例

### 手机

![demo](https://gitee.com/johnlui/Swift-MMP/raw/master/assets/demo.jpg)

### Apple Watch

![demo](https://gitee.com/johnlui/Swift-MMP/raw/master/assets/watch-demo.jpg)

## 环境要求

* iOS 9.3+
* Xcode 8

## 参与开源

欢迎提交 issue 和 PR，大门永远向所有人敞开。

## 开源协议

本项目遵循 MIT 协议开源，具体请查看根目录下的 LICENSE 文件。